import 'package:flutter/widgets.dart';

class Icons_Mute {
  Icons_Mute._();

  static const _kFontFam = 'Icons_Mute';
  static const String _kFontPkg = null;

  static const IconData mute_fil_16 = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}