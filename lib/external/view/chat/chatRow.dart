import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:fluffychat/external/helper/Avatarka.dart';
import 'package:fluffychat/external/helper/icons_mute.dart';
import 'package:fluffychat/pages/send_file_dialog.dart';
import 'package:fluffychat/utils/matrix_sdk_extensions.dart/matrix_locals.dart';
import 'package:fluffychat/widgets/matrix.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart' as DefWidgets;
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix/matrix.dart';
import 'package:vrouter/vrouter.dart';
import '../../helper/counter.dart';
import '../../helper/colors.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:pedantic/pedantic.dart';

enum ArchivedRoomAction { delete, rejoin }

class ChatRow extends StatelessWidget {
  // Chat toRow;
  // User user = me;

  final Room room;
  final bool activeChat;
  final bool selected;
  final Function onForget;
  final Function onTap;
  final Function onLongPress;

  const ChatRow(this.room,{
    this.activeChat,
    this.selected,
    this.onTap,
    this.onLongPress,
    this.onForget
  });

  void clickAction(BuildContext context) async {
    if (onTap != null) return onTap();
    if (!activeChat) {
      if (room.membership == Membership.invite &&
          (await showFutureLoadingDialog(
              context: context, future: () => room.join()))
              .error !=
              null) {
        return;
      }

      /*if (room.membership == Membership.ban) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context).youHaveBeenBannedFromThisChat),
          ),
        );
        return;
      }*/

      if (room.membership == Membership.leave) {
        final action = await showModalActionSheet<ArchivedRoomAction>(
          context: context,
          title: L10n.of(context).archivedRoom,
          message: L10n.of(context).thisRoomHasBeenArchived,
          actions: [
            SheetAction(
              label: L10n.of(context).rejoin,
              key: ArchivedRoomAction.rejoin,
            ),
            SheetAction(
              label: L10n.of(context).delete,
              key: ArchivedRoomAction.delete,
              isDestructiveAction: true,
            ),
          ],
        );
        if (action != null) {
          switch (action) {
            case ArchivedRoomAction.delete:
              await archiveAction(context);
              break;
            case ArchivedRoomAction.rejoin:
              await showFutureLoadingDialog(
                context: context,
                future: () => room.join(),
              );
              break;
          }
        }
      }

      if (room.membership == Membership.join) {
        if (Matrix.of(context).shareContent != null) {
          if (Matrix.of(context).shareContent['msgtype'] ==
              'chat.fluffy.shared_file') {
            await showCupertinoDialog(
              context: context,
              useRootNavigator: false,
              builder: (c) => SendFileDialog(
                file: Matrix.of(context).shareContent['file'],
                room: room,
              ),
            );
          } else {
            unawaited(room.sendEvent(Matrix.of(context).shareContent));
          }
          Matrix.of(context).shareContent = null;
        }
        VRouter.of(context).to('/rooms/${room.id}');
      }
    }
  }

  Future<void> archiveAction(BuildContext context) async {
    {
      if ([Membership.leave, Membership.ban].contains(room.membership)) {
        final success = await showFutureLoadingDialog(
          context: context,
          future: () => room.forget(),
        );
        if (success.error == null) {
          if (onForget != null) onForget();
        }
        return success;
      }
      final confirmed = await showOkCancelAlertDialog(
        useRootNavigator: false,
        context: context,
        title: L10n.of(context).areYouSure,
        okLabel: L10n.of(context).yes,
        cancelLabel: L10n.of(context).no,
      );
      if (confirmed == OkCancelResult.cancel) return;
      await showFutureLoadingDialog(
          context: context, future: () => room.leave());
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    final isMuted = room.pushRuleState != PushRuleState.notify;
    final ownMessage =
        room.lastEvent?.senderId == Matrix.of(context).client.userID;
    final senderFIO = (room.lastEvent?.sender?.displayName ?? '').split(' ');
    String sender;
    // print(room.toJson().toString());
    if (!room.isDirectChat && senderFIO.length > 1) {
      sender = senderFIO.first + ' ' +  senderFIO.last[0] + ': ';
    } else {
      if (ownMessage) {
        sender = 'Вы: ';
      } else {
        sender = '';
      }
    }
    final time = room.timeCreated.difference(DateTime.now()).inDays.abs() > 0
        ? room.timeCreated.difference(DateTime.now()).inDays.abs().toString() + 'д.'
        : '${room.timeCreated.hour}:${room.timeCreated.minute}';
    return GestureDetector(
      onTap: () {
        /*Navigator.push(context,
            CupertinoPageRoute(builder: (context) => MessagesView(user, toRow))

            ///!!!!
            );*/
        clickAction(context);
      },
      onLongPress: onLongPress,
      child: Container(
        margin: EdgeInsets.only(bottom: 16),
          child: Row(

        children: [
          Avatarka(room, isOnline: true,),
          Expanded(
            flex: 5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    child: Row(
                      children: [
                        Flexible(
                          child: Text(
                            // toRow.getChatName(user.userID),
                            room.getLocalizedDisplayname(MatrixLocals(L10n.of(context))),
                            overflow: TextOverflow.clip,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                              fontFamily: 'SFProText',
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              color: DarkText.primary,
                              decoration: TextDecoration.none
                            ),
                          ),
                        ),
                        /*Text(
                          '...',
                          style: TextStyle(
                              fontFamily: 'SFProText',
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              color: darkText_primary,
                              decoration: TextDecoration.none
                          ),
                        ),*/
                        DefWidgets.Visibility(
                          visible: isMuted/*user.isMute(toRow.chatName)*/,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(4, 3, 0, 3),
                            child: Icon(
                              Icons_Mute.mute_fil_16,
                              size: 16,
                              color: Icons.inactive,
                            ),
                          ),
                        )
                      ],
                    )),
                Row(children: [
                  Container(
                    child: Text(
                      sender ?? '',
                      // (isFromMe(user.userID, toRow.messageList.last.senderID)
                      //     ? 'Вы: '
                      //     : NameView(toRow.messageList.last.senderID) + ': '),
                      overflow: TextOverflow.clip,
                      maxLines: 1,
                      softWrap: false,
                      style: TextStyle(
                        fontFamily: 'SFProText',
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: DarkText.tertiary,
                        decoration: TextDecoration.none

                      ),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      child: Text(
                        room.lastEvent?.getLocalizedBody(
                          MatrixLocals(L10n.of(context)),
                          hideReply: false,
                        ) ??
                            '',
                        // toRow.messageList.last.messageText,
                        overflow: TextOverflow.clip,
                        maxLines: 1,
                        softWrap: false,
                        style: TextStyle(
                          fontFamily: 'SFProText',
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          color: DarkText.secondary,
                          decoration: TextDecoration.none
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 6),
                    child: Text(
                      '· $time',
                      style: TextStyle(
                          fontFamily: 'SFProText',
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: DarkText.tertiary,
                          decoration: TextDecoration.none
                      ),
                    ),
                  ),
                ])
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: DefWidgets.Visibility(
              visible: room.notificationCount > 0 /*!toRow.messageList.last.isRead(user.userID)*/,
              child: Align(
                  child: Counter(
                      2, room.notificationCount
                    /*toRow.unreadCount(user.userID)*/
                  )
              ),
            ),
          )
        ],
      )),
    );
  }
}

String NameView(String fullname) {
  return fullname.substring(0, fullname.indexOf(' ') + 2) + '.';
}

bool isFromMe(String user, String sender) {
  return user == sender;
}
